# marketplace
Vendor Marketplace API demo to perform REST calls to Accounts and Contacts

TO RUN:
1. Import as a Gradle project
2. In the root folder, run with command:
gradle bootRun
3. Access this URL in your browser to see the Swagger UI of the exposed API endpoints:
http://localhost:8080/swagger-ui.html

TROUBLESHOOTING GRADLE:
If facing Gradle issues on MAC:
    a. Manually download Gradle to /opt/gradle
    b. Update PATH by adding this line to .bash_profile
        export PATH=$PATH:/opt/gradle/gradle-4.10/bin
    c. Save the changes with command:
        source .bash_profile
    d. Re-import the project in IntelliJ and point to the manually installed Gradle path during set-up

DATABASE:
1. Open this URL
http://localhost:8080/h2-console
2. For the JDBC URL use
jdbc:h2:mem:testdb
3. Click Connect
4. Click ACCOUNTS and click Run
5. Notice that the values that are already loaded in memory are because of these files located in /resources
schema.sql, data.sql

API AND CONTROLLER INFO:
Accounts can have one-to-many Contacts.
Contacts do not have to have an Account.
These endpoints are based on best practices:
https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design

ACCOUNTS API
1. To GET all Accounts:
http://localhost:8080/vendor/marketplace/accounts
2. To CRUD a specific Account:
http://localhost:8080/vendor/marketplace/accounts/{accountId}
3. To GET all Account's Contacts:
http://localhost:8080/vendor/marketplace/accounts/{accountId}/contacts

CONTACTS API
1. To GET all Contacts
http://localhost:8080/vendor/marketplace/contacts
2. To CRUD a specific Contact:
http://localhost:8080/vendor/marketplace/contacts/{contactId}

TEST WITH JUNIT:
1. Run this command in workspace terminal
./gradlew clean test --info

