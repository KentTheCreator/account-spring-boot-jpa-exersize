package com.atlassian.vendor.marketplace;

public class Constants {
	public static final String SUCCESS = "success";
	public static final String FAILED = "failed";
	public static final String PACKAGE_MODEL = "com.atlassian.vendor.marketplace.model.";
}
