package com.atlassian.vendor.marketplace;

import com.atlassian.vendor.marketplace.model.entity.Account;
import com.atlassian.vendor.marketplace.model.entity.Contact;
import com.atlassian.vendor.marketplace.service.AccountDaoService;
import com.atlassian.vendor.marketplace.service.ContactDaoService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Generates demo data at runtime.
 * Creates and queries the JPA Account and Contact repositories.
 * This can then be seen in the H2 in-memory database or from the REST endpoints.
 * Please see README.md for steps.
 */
@Component
public class MarketplaceCommandLineRunner implements CommandLineRunner {

	private static final Logger LOGGER = LogManager.getLogger(MarketplaceCommandLineRunner.class);

	@Autowired
	private AccountDaoService accountDaoService;

	@Autowired
	private ContactDaoService contactDaoService;

	/**
	 * Generate some data to see with Swagger, Postman, H2 login, or browser
	 * @param arg0
	 * @throws Exception
	 */
	@Override
	public void run(String... arg0) throws Exception {

		//create some new Accounts
		Account account1 = new Account("Cadillac","123 Address","Suite F","Austin","TX","78723","USA");
		long accountId1 = accountDaoService.insert(account1);
		LOGGER.info("INFO: NEW ACCOUNT CREATED: " + account1 + " new accountId: " + accountId1);

		Account account2 = new Account("Chevrolet","123 Address","Suite F","Austin","TX","78723","USA");
		long accountId2 = accountDaoService.insert(account2);
		LOGGER.info("INFO: NEW ACCOUNT CREATED: " + account2 + " new accountId: " + accountId2);

		//query Account to find all Accounts
		List<Account> accounts = accountDaoService.findAll();
		LOGGER.info("INFO: FIND ALL ACCOUNTS: " + accounts);

		//query Account to find Account by generated accountId 1
		Account uniqueAccount = accountDaoService.findById(1L);
		LOGGER.info("INFO: FIND ACCOUNTS BY ID 1: " + uniqueAccount);

		//add Contacts to 1st Account
		Contact contact1 = new Contact(1L, "Johan de Nysschen", "jdn@cadillac.com", "330 Hudson Street", "", "New York", "NY", "10013", "USA");
		long contactId1 = contactDaoService.insert(contact1);
		LOGGER.info("INFO: NEW CONTACT CREATED: " + contact1 + " new contactId: " + contactId1);

		Contact contact2 = new Contact(1L, "Deborah Wahl", "deb@cadillac.com", "330 Hudson Street", "", "New York", "NY", "10013", "USA");
		long contactId2 = contactDaoService.insert(contact2);
		LOGGER.info("INFO: NEW CONTACT CREATED: " + contact2 + " new contactId: " + contactId2);

		//add Contact to 2nd Account
		Contact contact3 = new Contact(2L, "Tom Bolton", "tb@chevrolet.com", "330 Hudson Street", "", "Detroit", "MI", "10013", "USA");
		long contactId3 = contactDaoService.insert(contact3);
		LOGGER.info("INFO: NEW CONTACT CREATED: " + contact3 + " new contactId: " + contactId3);

//		//TODO find a way to add a Contact without an Account ID
//		Contact contact4 = new Contact(5L, "Iggy Pop", "ip@gmail.com", "330 Hudson Street", "", "Detroit", "MI", "10013", "USA");
//		long contactId4 = contactDaoService.insert(contact4);
//		LOGGER.info("INFO: NEW CONTACT CREATED: " + contact4 + " new contactId: " + contactId4);

	}

}
