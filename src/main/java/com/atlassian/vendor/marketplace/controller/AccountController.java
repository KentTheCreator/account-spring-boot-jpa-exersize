package com.atlassian.vendor.marketplace.controller;

import com.atlassian.vendor.marketplace.Constants;
import com.atlassian.vendor.marketplace.model.entity.Account;
import com.atlassian.vendor.marketplace.model.entity.Contact;
import com.atlassian.vendor.marketplace.model.response.AccountResponse;
import com.atlassian.vendor.marketplace.model.response.ContactResponse;
import com.atlassian.vendor.marketplace.model.response.Data;
import com.atlassian.vendor.marketplace.service.AccountDaoService;
import com.atlassian.vendor.marketplace.service.ContactDaoService;
import com.atlassian.vendor.marketplace.util.FormatUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller to interact with Accounts using CRUD REST Methods
 * Account can have one-to-many contacts
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/vendor/marketplace/accounts")
public class AccountController {

	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);

	@Autowired
	private AccountDaoService accountDaoService;

	@Autowired
	private ContactDaoService contactDaoService;

	/**
	 * @return all Accounts
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<AccountResponse> getAllAccounts() {
		AccountResponse accountResponse = new AccountResponse();
		Data data = new Data();
		List<Account> accounts = accountDaoService.findAll();
		if (accounts.isEmpty()) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}
		accountResponse.setStatus(Constants.SUCCESS);
		data.setAccounts(accounts);
		accountResponse.setData(data);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

	/**
	 * @return status if new Account was created
	 */
	@PostMapping()
	public final ResponseEntity<AccountResponse> addNewAccount(@RequestBody Account account) {
		AccountResponse accountResponse = new AccountResponse();

		//check formatting of these inputs
		if (!FormatUtil.isStateFormatValid(account.getState())
				|| !FormatUtil.isCountryFormatValid(account.getCountry())) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		//check if the Company Name already exists
		if (!accountDaoService.findAllAccountCompanyNames(account.getCompanyName()).isEmpty()) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}
		account = new Account(account.getCompanyName(), account.getAddressLine1(), account.getAddressLine2(),
				account.getCity(), account.getState(), account.getPostalCode(), account.getCountry());
		long accountId = accountDaoService.insert(account);
		if (accountId == 0) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}
		accountResponse.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

	/**
	 * @return status if existing Account was updated
	 */
	@PutMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<AccountResponse> updateExistingAccount(@PathVariable(name = "accountId") String accountId,
																	   @RequestBody Account account) {
		AccountResponse accountResponse = new AccountResponse();

		//check if the Account exists
		Account uniqueAccount = accountDaoService.findById(Integer.parseInt(accountId));
		if (uniqueAccount == null) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}

		//update any non-null values
		accountDaoService.updateAccountById(Integer.parseInt(accountId), account);
		accountResponse.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

	/**
	 * @param accountId long
	 * @return unique Account ID
	 */
	@GetMapping(value = "/{accountId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<AccountResponse> getAccountById(@PathVariable(name = "accountId") String accountId) {
		AccountResponse accountResponse = new AccountResponse();
		Data data = new Data();
		Long convertedAccountId;
		try {
			convertedAccountId = Long.parseLong(accountId);
		} catch (NumberFormatException e) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Account uniqueAccount = accountDaoService.findById(convertedAccountId);
		if (uniqueAccount == null) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}
		List<Account> accounts = new ArrayList<>();
		accounts.add(uniqueAccount);
		accountResponse.setStatus(Constants.SUCCESS);
		data.setAccounts(accounts);
		accountResponse.setData(data);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

	/**
	 * @param accountId long
	 * @return all Contacts associated with unique Account ID
	 */
	@GetMapping(value = "/{accountId}/contacts", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<ContactResponse> getAllContactsByAccountId(@PathVariable(name = "accountId") String accountId) {
		ContactResponse contactResponse = new ContactResponse();
		Data data = new Data();
		Long convertedAccountId;
		try {
			convertedAccountId = Long.parseLong(accountId);
		} catch (NumberFormatException e) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		List<Contact> contacts = contactDaoService.findAllContactsForAccountId(convertedAccountId);
		if (contacts == null || contacts.isEmpty()) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.NOT_FOUND);
		}
		contactResponse.setStatus(Constants.SUCCESS);
		data.setContacts(contacts);
		contactResponse.setData(data);
		return new ResponseEntity<>(contactResponse, HttpStatus.OK);
	}

	/**
	 * @param accountId long
	 * @return status if Account could be deleted
	 */
	@DeleteMapping(value = "/{accountId}")
	public final ResponseEntity<AccountResponse> deleteAccountById(@PathVariable(name = "accountId") String accountId) {
		AccountResponse accountResponse = new AccountResponse();
		Long convertedAccountId;
		try {
			convertedAccountId = Long.parseLong(accountId);
		} catch (NumberFormatException e) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Contact uniqueAccount = contactDaoService.findById(convertedAccountId);
		if (uniqueAccount == null) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}
		accountResponse.setStatus(Constants.SUCCESS);
		accountDaoService.removeAccountById(convertedAccountId);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

}
