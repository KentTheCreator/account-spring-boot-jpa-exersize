package com.atlassian.vendor.marketplace.controller;

import com.atlassian.vendor.marketplace.Constants;
import com.atlassian.vendor.marketplace.model.entity.Contact;
import com.atlassian.vendor.marketplace.model.response.AccountResponse;
import com.atlassian.vendor.marketplace.model.response.ContactResponse;
import com.atlassian.vendor.marketplace.model.response.Data;
import com.atlassian.vendor.marketplace.service.ContactDaoService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller to interact with Contacts using CRUD REST Methods
 * Contact can have one-to-none account
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/vendor/marketplace/contacts")
public class ContactController {

	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);

	@Autowired
	private ContactDaoService contactDaoService;

	/**
	 * @return all Contacts
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<ContactResponse> getAllContacts() {
		ContactResponse contactResponse = new ContactResponse();
		Data data = new Data();
		List<Contact> contacts = contactDaoService.findAll();
		if (contacts.isEmpty()) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.NOT_FOUND);
		}
		contactResponse.setStatus(Constants.SUCCESS);
		data.setContacts(contacts);
		contactResponse.setData(data);
		return new ResponseEntity<>(contactResponse, HttpStatus.OK);
	}

	/**
	 * @return status if new Contact was created
	 */
	@PostMapping()
	public final ResponseEntity<ContactResponse> addNewContact(@RequestBody Contact contact) {
		ContactResponse contactResponse = new ContactResponse();

		contact = new Contact(contact.getAccountId(), contact.getName(), contact.getEmailAddress(),
				contact.getAddressLine1(), contact.getAddressLine2(), contact.getCity(), contact.getState(),
				contact.getPostalCode(), contact.getCountry());
		long contactId = contactDaoService.insert(contact);
		if (contactId == 0) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.NOT_FOUND);
		}
		contactResponse.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(contactResponse, HttpStatus.OK);
	}

	/**
	 * @return status if existing Contact was updated
	 */
	@PutMapping(value = "/{contactId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<AccountResponse> updateExistingAccount(@PathVariable(name = "contactId") String contactId,
																	   @RequestBody Contact contact) {
		AccountResponse accountResponse = new AccountResponse();

		//check if the Contact exists
		Contact uniqueAccount = contactDaoService.findById(Integer.parseInt(contactId));
		if (uniqueAccount == null) {
			accountResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(accountResponse, HttpStatus.NOT_FOUND);
		}

		//update any non-null values
		contactDaoService.updateContactById(Integer.parseInt(contactId), contact);
		accountResponse.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(accountResponse, HttpStatus.OK);
	}

	/**
	 * @param contactId long
	 * @return all Contacts found with unique Contact ID
	 */
	@GetMapping(value = "/{contactId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final ResponseEntity<ContactResponse> getAllContactsByAccountId(@PathVariable(name = "contactId") String contactId) {
		ContactResponse contactResponse = new ContactResponse();
		Long convertedAccountId;
		try {
			convertedAccountId = Long.parseLong(contactId);
		} catch (NumberFormatException e) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Contact uniqueAccount = contactDaoService.findById(convertedAccountId);
		if (uniqueAccount == null) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.NOT_FOUND);
		}
		Data data = new Data();
		List<Contact> contacts = new ArrayList<>();
		contacts.add(uniqueAccount);
		contactResponse.setStatus(Constants.SUCCESS);
		data.setContacts(contacts);
		contactResponse.setData(data);
		return new ResponseEntity<>(contactResponse, HttpStatus.OK);
	}

	/**
	 * @param contactId long
	 * @return status if Contact could be deleted
	 */
	@DeleteMapping(value = "/{contactId}")
	public final ResponseEntity<ContactResponse> deleteContactById(@PathVariable(name = "contactId") String contactId) {
		ContactResponse contactResponse = new ContactResponse();
		Long convertedContactId;
		try {
			convertedContactId = Long.parseLong(contactId);
		} catch (NumberFormatException e) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Contact uniqueAccount = contactDaoService.findById(convertedContactId);
		if (uniqueAccount == null) {
			contactResponse.setStatus(Constants.FAILED);
			return new ResponseEntity<>(contactResponse, HttpStatus.NOT_FOUND);
		}
		contactDaoService.removeContactById(convertedContactId);
		contactResponse.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(contactResponse, HttpStatus.OK);
	}


}
