package com.atlassian.vendor.marketplace.model.response;

import com.atlassian.vendor.marketplace.Constants;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Wrapper class for to hold Status and Data if available
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse implements Serializable {
	private static final long serialVersionUID = -5287317969010316625L;

	private String status;
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private void readObject(java.io.ObjectInputStream in) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "BaseResponse");
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "BaseResponse");
	}

}
