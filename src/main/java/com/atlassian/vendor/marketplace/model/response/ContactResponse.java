package com.atlassian.vendor.marketplace.model.response;

import com.atlassian.vendor.marketplace.Constants;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * Wrapper class for Contact table entities
 * to show status and optional Data from DB
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactResponse extends BaseResponse implements Serializable {
	private static final long serialVersionUID = 7221053149387287034L;

	private void readObject(java.io.ObjectInputStream in) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "ContactData");
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "ContactData");
	}
}
