package com.atlassian.vendor.marketplace.model.response;

import com.atlassian.vendor.marketplace.Constants;
import com.atlassian.vendor.marketplace.model.entity.Account;
import com.atlassian.vendor.marketplace.model.entity.Contact;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * Wrapper class for Account or Contact table entities
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data implements Serializable {
	private static final long serialVersionUID = 3293433887856887511L;

	private List<Account> accounts;
	private List<Contact> contacts;

	public  List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> account) {
		this.accounts = account;
	}

	public  List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contact) {
		this.contacts = contact;
	}

	private void readObject(java.io.ObjectInputStream in) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "Data");
	}

	private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
		throw new java.io.NotSerializableException(Constants.PACKAGE_MODEL + "Data");
	}
}
