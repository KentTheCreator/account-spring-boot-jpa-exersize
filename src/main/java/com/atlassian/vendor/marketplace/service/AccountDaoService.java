package com.atlassian.vendor.marketplace.service;

import com.atlassian.vendor.marketplace.controller.AccountController;
import com.atlassian.vendor.marketplace.model.entity.Account;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Used to query persisted Account repository
 */
@Repository
@Transactional
public class AccountDaoService {

	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);

	@PersistenceContext
	private EntityManager entityManager;

	public long insert(Account account) {
		entityManager.persist(account);
		return account.getAccountId();
	}

	public List<Account> findAll() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
		Root<Account> rootEntry = criteriaQuery.from(Account.class);
		CriteriaQuery<Account> all = criteriaQuery.select(rootEntry);
		TypedQuery<Account> allQuery = entityManager.createQuery(all);
		return allQuery.getResultList();
	}

	public Account findById(long id) {
		return entityManager.find(Account.class, id);
	}

	public void removeAccountById(long id) {
		Account account = entityManager.find(Account.class, id);
		entityManager.remove(account);
	}

	public List<Account> findAllAccountCompanyNames(String companyName) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
		Root<Account> rootEntry = criteriaQuery.from(Account.class);
		criteriaQuery.select(rootEntry).where(criteriaBuilder.equal(rootEntry.get("companyName"), companyName));
		TypedQuery<Account> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

	//TODO there's gotta be a better way than this - check commented out code below for progress
	public void updateAccountById(long id, Account account) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaUpdate<Account> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(Account.class);
		Root<Account> rootEntry = criteriaUpdate.from(Account.class);

		if (account.getCompanyName() != null) {
			criteriaUpdate.set("companyName", account.getCompanyName())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getAddressLine1() != null) {
			criteriaUpdate.set("addressLine1", account.getAddressLine1())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getAddressLine2() != null) {
			criteriaUpdate.set("addressLine2", account.getAddressLine2())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getCity() != null) {
			criteriaUpdate.set("city", account.getCity())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getState() != null) {
			criteriaUpdate.set("state", account.getState())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getPostalCode() != null) {
			criteriaUpdate.set("postalCode", account.getPostalCode())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (account.getCountry() != null) {
			criteriaUpdate.set("country", account.getCountry())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		this.entityManager.createQuery(criteriaUpdate).executeUpdate();
	}

//	private String[] getNullPropertyNames(Object source) {
//		final BeanWrapper src = new BeanWrapperImpl(source);
//		java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
//
//		Set<String> emptyNames = new HashSet<>();
//		for (java.beans.PropertyDescriptor pd : pds) {
//			Object srcValue = src.getPropertyValue(pd.getName());
//
//			if (srcValue != null
//					&& !pd.getName().equals("class")
//					&& !pd.getName().equals("accountId")) {
//				emptyNames.add(pd.getName());
//			}
//		}
//		String[] result = new String[emptyNames.size()];
//		return emptyNames.toArray(result);
//	}

	//WIP
//	public void updateAccountById(long id, Account account) {
//		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Account> query = criteriaBuilder.createQuery(Account.class);
//
//		//update all fields if available
//		Metamodel m = entityManager.getMetamodel();
//		EntityType<Account> entity = m.entity(Account.class);
//		Root model = query.from(Account.class);
//		query.where(criteriaBuilder.notEqual(model.get(account.getCompanyName()), ""));
//	}



}
