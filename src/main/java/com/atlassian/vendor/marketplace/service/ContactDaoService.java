package com.atlassian.vendor.marketplace.service;

import com.atlassian.vendor.marketplace.controller.AccountController;
import com.atlassian.vendor.marketplace.model.entity.Contact;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Used to query persisted Contact repository
 */
@Repository
@Transactional
public class ContactDaoService {

	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);

	@PersistenceContext
	private EntityManager entityManager;

	public long insert(Contact contact) {
		entityManager.persist(contact);
		return contact.getContactId();
	}

	public List<Contact> findAll() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
		Root<Contact> rootEntry = criteriaQuery.from(Contact.class);
		CriteriaQuery<Contact> all = criteriaQuery.select(rootEntry);
		TypedQuery<Contact> allQuery = entityManager.createQuery(all);
		return allQuery.getResultList();
	}

	//criteriaQuery is interesting to say the least
	public List<Contact> findAllContactsForAccountId(long id) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
		Root<Contact> rootEntry = criteriaQuery.from(Contact.class);
		criteriaQuery.select(rootEntry).where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		TypedQuery<Contact> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

	public Contact findById(long id) {
		return entityManager.find(Contact.class, id);
	}

	public void removeContactById(long id) {
		Contact contact = entityManager.find(Contact.class, id);
		entityManager.remove(contact);
	}

	//TODO there's gotta be a better way than this - check commented out code in AccountDaoService for progress
	public void updateContactById(long id, Contact contact) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaUpdate<Contact> criteriaUpdate = criteriaBuilder.createCriteriaUpdate(Contact.class);
		Root<Contact> rootEntry = criteriaUpdate.from(Contact.class);

		if (contact.getName() != null) {
			criteriaUpdate.set("name", contact.getName())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getEmailAddress() != null) {
			criteriaUpdate.set("emailAddress", contact.getEmailAddress())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getAddressLine1() != null) {
			criteriaUpdate.set("addressLine1", contact.getAddressLine1())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getAddressLine2() != null) {
			criteriaUpdate.set("addressLine2", contact.getAddressLine2())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getCity() != null) {
			criteriaUpdate.set("city", contact.getCity())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getState() != null) {
			criteriaUpdate.set("state", contact.getState())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getPostalCode() != null) {
			criteriaUpdate.set("postalCode", contact.getPostalCode())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		if (contact.getCountry() != null) {
			criteriaUpdate.set("country", contact.getCountry())
					.where(criteriaBuilder.equal(rootEntry.get("accountId"), id));
		}
		this.entityManager.createQuery(criteriaUpdate).executeUpdate();
	}

}
