package com.atlassian.vendor.marketplace.util;

/**
 * Utility to validate inputs for DB insert
 */
public class FormatUtil {

	private FormatUtil() {}

	/**
	 * @param state in country
	 * @return if state is 2 alpha characters
	 */
	public static boolean isStateFormatValid(String state) {
		return state.toLowerCase().matches("^[a-z]{2}$");
	}

	/**
	 * @param country of account or contact
	 * @return if country is 2 to 3 alpha characters
	 */
	public static boolean isCountryFormatValid(String country) {
		return country.toLowerCase().matches("^[a-z]{2,3}$");
	}

}
