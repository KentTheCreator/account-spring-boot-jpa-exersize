-- Using MarketplaceCommandLineRunner to insert instead
-- Useful for quickly inserting into H2 DB and using JDBC Template to query
-- However, Account must be changed to a Component instead of an Entity

-- note: this is just used for demonstration purposes
-- commenting out everything throws a null
-- insert into account
values(1,'HEB','123 Address','Suite F','Austin','TX','78723','USA'),
      (2,'GM','123 Address','Suite F','Austin','TX','78723','USA');

-- insert into contact
-- values('1','Mike Georgoff','mike.georgoff@heb.com','123 Address','Suite F','Austin','TX','78723','USA'),
--       ('2','GM','kent.lybrook@gmail.com','123 Address','Suite F','Austin','TX','78723','USA');