-- Using MarketplaceCommandLineRunner to insert instead
-- Useful for quickly creating a table for H2 DB
-- However, Account must be changed to a Component instead of an Entity

-- note: this is just used for demonstration purposes
-- commenting out everything throws a null
create table account
(
  accountId int not null,
  companyName varchar(255) not null,
  addressLine1 varchar(255) not null,
  addressLine2 varchar(255) not null,
  city varchar(255) not null,
  state varchar(255) not null,
  postalCode varchar(255) not null,
  country varchar(255) not null,
  primary key(accountId)
);

-- create table contact
-- (
--   name varchar(255) not null,
--   emailAddress varchar(255) not null,
--   addressLine1 varchar(255) not null,
--   addressLine2 varchar(255) not null,
--   city varchar(255) not null,
--   state varchar(255) not null,
--   postalCode varchar(255) not null,
--   country varchar(255) not null,
--   primary key(name)
-- );