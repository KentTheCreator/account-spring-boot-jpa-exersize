package com.atlassian.vendor.marketplace;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class CommonUtil {

	public static HttpHeaders createHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.add("locale", locale);
		return headers;
	}

	public static String createUrlForControllerPath(Integer port, String uriPath) {
		return "http://localhost:" + port + "/vendor/marketplace/" + uriPath;
	}
}
